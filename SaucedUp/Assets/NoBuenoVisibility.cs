﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoBuenoVisibility : MonoBehaviour
{

    private float displayTimer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
        enabled = false;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        displayTimer -= Time.deltaTime;

        if (displayTimer <= 0f)
        {
            enabled = false;
            gameObject.SetActive(false);
        }
    }

    public void RecipeFinished(SNS.Event.EventArgs evt)
    {
        displayTimer = 1.5f;
        enabled = true;
        gameObject.SetActive(true);
    }

}
