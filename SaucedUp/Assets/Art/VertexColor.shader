﻿Shader "Custom/VertexColor" {

	Properties
	{
		_Tintcolor ("ColorTint", Color) = (0,0,0,0)
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert
		#pragma target 3.0

		struct Input {
			float4 vertColor;
		};

		float4 _Tintcolor; 

		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = IN.vertColor.rgb + _Tintcolor;
		}
		ENDCG
	}
	FallBack "Diffuse"
}