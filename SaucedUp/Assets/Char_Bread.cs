﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Char_Bread : MonoBehaviour
{
  [SerializeField] private Vector3 spawnPosition = default;

    //public InputHandler inputHnd;
    public string myName;
    public float speed;
    private float origSpeed;
    public Rigidbody body;
    public int playerId;
    private Transform PlayAreaTransform = default;
    private float sceneCentre;
    private CharacterController controller;

    public float minimumRunTime = 0.5f;
    private float currentRunTime = 0f;
    private Vector3 gravity = new Vector3(0, -9.81f, 0);
    [SerializeField]  private Vector3 push = Vector3.zero;
    private float respawningTimeout = 0.0f;

    public float zOffset;
    public Animator animator;
    public bool isPaused = false;
    public bool isHeadButting = false;

    private float lastHeadButtTime = 0;
    private const float HEAD_BUTT_TIME = 1.25f;
    [SerializeField] private const float HEAD_BUTT_COOLDOWN_TIME = 1.0f;

    private Material myMaterial;
    [SerializeField] private float slowDownFactor = default;

    // Start is called before the first frame update
    void Start()
    {
        //inputHnd = InputHandler.Instance;
        Debug.Log("Calling start for " + myName);
        controller = gameObject.GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        PlayAreaTransform = GameObject.Find("Play Area").transform;
        sceneCentre = PlayAreaTransform.position.z + zOffset;

        origSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        float dTime = Time.deltaTime;
        //         float smooth = 20f;
        //         float rotAngle = 60f;

        if (isPaused) return; // don't allow movement if we're paused

        if (Time.time - lastHeadButtTime > HEAD_BUTT_TIME && isHeadButting)
        {
            isHeadButting = false;
            Debug.Log("Stop Headbutting");
        }

        //inputHnd.Evaluate();

        if (respawningTimeout <= 0.0f)
        {
            Vector3 move = default;
            bool grabSauce = false;
            if (playerId == 0)
            {
                move = new Vector3(-Input.GetAxis("Vertical"), 0, Input.GetAxis("Horizontal"));
                if (Input.GetButtonDown("CROSS") && Time.time - lastHeadButtTime > HEAD_BUTT_COOLDOWN_TIME)
                {
                    grabSauce = true;
                }
            }
            else
            {
                move = new Vector3(-Input.GetAxis("Vertical1"), 0, Input.GetAxis("Horizontal1"));
                if (Input.GetKeyDown(KeyCode.Space) && Time.time - lastHeadButtTime > HEAD_BUTT_COOLDOWN_TIME)
                {
                    grabSauce = true;
                }
            }

            // let's slow me down based on number of ingredients
            

            // clamp to my area
            Vector3 movement = (move + gravity + push) * Time.deltaTime * speed;
            controller.Move(movement);

            Vector3 clampedTransform = controller.transform.position;
            clampedTransform.z = Mathf.Clamp(clampedTransform.z, playerId * sceneCentre, (playerId * sceneCentre) + sceneCentre);
            controller.transform.position = clampedTransform;
                
            if (grabSauce)
            {
                animator.SetTrigger("grabSauce");
                isHeadButting = true;
                lastHeadButtTime = Time.time;
                Debug.Log("Headbutting");
                StartCoroutine(Blow());
            }

            if (move != Vector3.zero)
            {
                gameObject.transform.forward = move;
                currentRunTime = minimumRunTime;
            }
            currentRunTime -= dTime;

            animator.SetFloat("currentRunTime", currentRunTime);

        }
        else
        {
            respawningTimeout -= Time.deltaTime;
            if (respawningTimeout <= 0.0f)
            {
              transform.position = spawnPosition;
            }
        }
        push *= 1.0f * Time.deltaTime;

    }

    public void Respawn(Vector3 spawn)
    {
        // TODO PlayDeathAnimation
        respawningTimeout = 0.1f;
        spawnPosition = spawn;
        transform.position = spawn;
		speed = origSpeed;
    }

    public void Push(Vector3 dir)
    {
        push = dir;
    }

    public void SlowDown()
    {
        speed *= slowDownFactor;
        // but let's clamp it so we don't get ridiculously too slow that he can't incinerate himself
        speed = Mathf.Clamp(speed, 0.6f*origSpeed, origSpeed);
    }

    IEnumerator Blow()
    {
        yield return new WaitForSeconds(0.2f);
        // Load the vfx
        GameObject instance = (GameObject)Instantiate(Resources.Load("Prefabs/Headbutt_Effect"));
        instance.transform.position = new Vector3(transform.position.x, transform.position.y + 0.75f, transform.position.z);
        instance.transform.rotation = transform.rotation;
        instance.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(0.3f);        

        RaycastHit hit;
        int throwableMask = LayerMask.GetMask("GameItem");
        if (Physics.SphereCast(transform.position, 1.0f, transform.forward, out hit, 1.0f, throwableMask))
        {
            hit.rigidbody.AddForce(transform.forward * 10.0f, ForceMode.Impulse);

            
        }
    }
}
