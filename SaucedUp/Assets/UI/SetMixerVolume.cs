﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMixerVolume : SetMixerParameter
{
    public void SetVolume(float volume)
    {
        SetParameter(Mathf.Log10(volume) * 20);
    }
}
