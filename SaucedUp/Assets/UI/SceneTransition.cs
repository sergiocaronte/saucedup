﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : Transition
{
    public string _sceneToLoad;

    override public void TriggerTransition()
    {
        SceneManager.LoadScene(_sceneToLoad);
    }

    override public IEnumerator StartTransition()
    {
        yield return new WaitForEndOfFrame();
        SceneManager.LoadScene(_sceneToLoad);
    }
}
