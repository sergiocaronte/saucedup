﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventToUnityEvent : MonoBehaviour
{
    public EventNames _eventName;
    public UnityEvent _unityEvent;

    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(_eventName, OnEvent);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(_eventName, OnEvent);
    }

    private void OnEvent()
    {
        _unityEvent.Invoke();
    }
}
