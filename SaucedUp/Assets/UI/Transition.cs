﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public abstract class Transition : MonoBehaviour
{
    virtual public void TriggerTransition()
    {
        StartCoroutine(StartTransition());
    }

    public abstract IEnumerator StartTransition();
}
