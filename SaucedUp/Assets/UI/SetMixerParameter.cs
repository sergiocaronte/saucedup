﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SetMixerParameter : MonoBehaviour
{
    public AudioMixer _audioMixer;
    public string _parameterName;
    public void SetParameter(float value)
    {
        _audioMixer.SetFloat(_parameterName, value);
    }
}
