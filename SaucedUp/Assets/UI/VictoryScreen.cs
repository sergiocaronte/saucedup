﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScreen : MonoBehaviour
{
    #region -- Members --
    public Image[] stars;

    #endregion

    #region -- Unity Methods --
    void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.SHOW_SCORE, UpdateStars);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.SHOW_SCORE, UpdateStars);
    }
    #endregion

    #region -- Methods --
    private void UpdateStars()
    {
        Debug.LogFormat("Update Stars {0}", LevelManager.Instance.GetStarsCount());
        int starsCount = LevelManager.Instance.GetStarsCount();
        for (int i = 2; i >= 0; --i)
        {
            if (i > starsCount)
            {
                stars[i].color = Color.black;
            }
            else
            {
                stars[i].color = Color.white;
            }
        }
    }
    #endregion
}
