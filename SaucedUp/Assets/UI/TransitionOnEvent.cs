﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionOnEvent : MonoBehaviour
{
    public EventNames _eventName;
    public Transition _transition;

    private void OnEvent(SNS.Event.EventArgs evt)
    {
        StartCoroutine(_transition.StartTransition());
    }

    // Start is called before the first frame update
    void Start()
    {
        ServiceLocator.GetEventSystem().StartListening(_eventName, OnEvent);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
