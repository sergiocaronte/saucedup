﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CowntdownLogic : MonoBehaviour
{
    public Text _textBox;

    public void StartCowntdown()
    {
        StartCoroutine(CountdownSequence());
    }

    public IEnumerator CountdownSequence()
    {
        _textBox.text = "Starting Round";
        yield return new WaitForSeconds(1.5f);
        _textBox.text = "Bready!";
        yield return new WaitForSeconds(1);
        _textBox.text = "Set!";
        yield return new WaitForSeconds(1);
        _textBox.text = "GO!";
        yield return new WaitForSeconds(0.3f);
        ServiceLocator.GetEventSystem().TriggerEvent(EventNames.STARTING_COUNTDOWN_FINISHED);
        gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
