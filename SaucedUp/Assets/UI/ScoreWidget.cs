﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreWidget : MonoBehaviour
{
    #region -- Members --
    [SerializeField] Text points;

    #endregion

    #region -- Unity Methods --
    // Start is called before the first frame update
    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.SCORE_CHANGED, OnScoreChanged);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.SCORE_CHANGED, OnScoreChanged);
    }

    #endregion

    #region -- Methods --
    private void OnScoreChanged(SNS.Event.EventArgs evt)
    {
        points.text = ((int)evt.args[0]).ToString();
    }
    #endregion


}
