﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibilityTransition : Transition
{
    public float _timeToNewVisibility;
    public bool _newVisibility;
    public GameObject _objectToTransition;

    override public IEnumerator StartTransition()
    {
        yield return new WaitForSeconds(2);
        _objectToTransition.SetActive(_newVisibility);
    }
}
