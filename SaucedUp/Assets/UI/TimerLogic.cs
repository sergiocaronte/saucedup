﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TimerLogic : MonoBehaviour
{
    private Text _textBox;

    // Start is called before the first frame update
    void Start()
    {
        _textBox = gameObject.GetComponent<Text>();
    }

    readonly int SECONDS_OVER_MINUTE = 60;

    // Update is called once per frame
    void Update()
    {
        float remainingTime = LevelManager.Instance.remainingTime;
        float subSecondTime = remainingTime - Mathf.Floor(remainingTime);
        int totalSeconds = Mathf.FloorToInt(remainingTime);
        int minutes = totalSeconds / SECONDS_OVER_MINUTE;
        int subMinuteSeconds = totalSeconds % SECONDS_OVER_MINUTE;
        StringBuilder sb = new StringBuilder();
        if (minutes > 0)
        {
            if (minutes < 10)
            {
                sb.Append('0');
            }
            sb.Append(minutes);
            sb.Append(':');
            if (subMinuteSeconds < 10)
            {
                sb.Append('0');
            }
            sb.Append(subMinuteSeconds);
        }
        else
        {
            if (subMinuteSeconds < 10)
            {
                sb.Append('0');
            }
            sb.Append(remainingTime.ToString("F2"));
        }
        _textBox.text = sb.ToString();
    }
}
