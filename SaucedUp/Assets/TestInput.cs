﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestInput : MonoBehaviour
{
    public InputHandler inputHnd;
    public string myName;
    public Rigidbody body;

    // Start is called before the first frame update
    void Start()
    {
        inputHnd = InputHandler.Instance;
        Debug.Log("Calling start for " + myName);
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        inputHnd.Evaluate();
        if (inputHnd.IsPressed(CommandCode.CONFIRM))
        {
            Debug.Log("Pressed Confirm\n");
            body.MovePosition(transform.position + new Vector3(10f, 0f, 0f));

        }

        if (inputHnd.IsPressed(CommandCode.MOUSELEFTPRESS))
        {
            Debug.Log("Pressed Mouse Left\n");
            body.MovePosition(transform.position + new Vector3(10f, 0f, 0f));

        }
    }
}
