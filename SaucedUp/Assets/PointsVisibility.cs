﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsVisibility : MonoBehaviour
{
    private float displayTimer = 0f;
    Text pointsText = default;

    // Start is called before the first frame update
    void Start()
    {
        enabled = false;
        gameObject.SetActive(false);
        pointsText = GetComponent<Text>();
    }
    // Update is called once per frame
    void Update()
    {
        displayTimer -= Time.deltaTime;

        if (displayTimer <= 0f)
        {
            enabled = false;
            gameObject.SetActive(false);
        }
    }

    public void RecipeFailed(SNS.Event.EventArgs evt)
    {
        pointsText.text = "0 Pts";
        DisplayPoints();
    }

    public void RecipeScored(SNS.Event.EventArgs evt)
    {
        RecipeSO recipe = evt.args[0] as RecipeSO;

        pointsText.text = recipe.Points + " Pts";
        DisplayPoints();
    }

    public void DisplayPoints()
    {
        displayTimer = 1.5f;
        enabled = true;
        gameObject.SetActive(true);
    }
}
