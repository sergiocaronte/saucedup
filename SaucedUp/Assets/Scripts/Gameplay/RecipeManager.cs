﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecipeManager : Singleton<RecipeManager>
{
    #region -- Members
    [SerializeField] List<RecipeSO> recipesPool = default;
    [SerializeField] RecipeSO firstRecipe = default;

    public List<RecipeSO> historyRecipes { get; private set; }
    public RecipeSO currentRecipe { get; private set; }

    private AudioClip successClip;
    private AudioClip failClip;

    private AudioSource audioSource;

    #endregion

    #region -- Unity Methods

    void Start()
    {
        audioSource = GameObject.Find("audsSfx").GetComponent<AudioSource>();

        successClip = Resources.Load<AudioClip>("AudioClips/Fox_Wonderful");
        failClip = Resources.Load<AudioClip>("AudioClips/Fox_OhhNo...");
    }

    #endregion

    #region -- Methods
    public void Initialize()
    {
        historyRecipes = new List<RecipeSO>();
        currentRecipe = firstRecipe;
        currentRecipe.Initialize();
        GetNewRecipe(0);
    }

    public void GetNewRecipe(int defaultRecipe = -1)
    {
        int recipeIndex = defaultRecipe;
        if (defaultRecipe == -1)
        {
            recipeIndex = UnityEngine.Random.Range(0, recipesPool.Count);
        }
        currentRecipe = recipesPool[recipeIndex];
        currentRecipe.Initialize();
        ServiceLocator.GetEventSystem().TriggerEvent(EventNames.NEW_RECIPE_PICKED, new SNS.Event.EventArgs(currentRecipe, recipeIndex));
    }

    public void CheckRecipe(BreadBehaviour p1, BreadBehaviour p2)
    {
        if (currentRecipe.IsRecipeCorrect(p1, p2))
        {
            Debug.Log("Recipe Right");
            ServiceLocator.GetEventSystem().TriggerEvent(EventNames.RECIPE_SCORED, new SNS.Event.EventArgs(currentRecipe));
            historyRecipes.Add(currentRecipe);
            GetNewRecipe(-1);
            audioSource.PlayOneShot(successClip, 1);
        }
        else
        {
            Debug.Log("Recipe Wrong");
            ServiceLocator.GetEventSystem().TriggerEvent(EventNames.RECIPE_FAILED, new SNS.Event.EventArgs(currentRecipe));
            audioSource.PlayOneShot(failClip, 1);
        }

        p1.Respawn(true);
        p2.Respawn(true);
    }
    #endregion
}
