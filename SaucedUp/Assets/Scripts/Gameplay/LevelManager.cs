﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    #region -- Members --
    public LevelTuningData levelData;

    public int score { get; private set; }

    public float stageTime { get; private set; }

    public float remainingTime { get { return Mathf.Max(levelData.levelDurationSec - stageTime,0.0f); } }

    private bool gameRunning = false;
    #endregion

    #region -- Unity Methods --
    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.STAGE_STARTED, OnGameStarted);
        ServiceLocator.GetEventSystem().StartListening(EventNames.RECIPE_SCORED, OnRecipeScored);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.STAGE_STARTED, OnGameStarted);
        ServiceLocator.GetEventSystem().StopListening(EventNames.RECIPE_SCORED, OnRecipeScored);
    }

    private void Start()
    {        
        stageTime = 0.0f;
    }

    private void Update()
    {
        if (!gameRunning)
        {
            return;
        }

        stageTime += Time.deltaTime;
        if (stageTime >= levelData.levelDurationSec)
        {
            gameRunning = false;
            Debug.Log("LEVEL HAS FINISHED!");
            ServiceLocator.GetEventSystem().TriggerEvent(EventNames.STAGE_ENDED);
        }
    }
    #endregion

    #region -- Methods --
    public float GetBallFrequency()
    {
        return levelData.GetBallFrequencyAt(stageTime/levelData.levelDurationSec);
    }

    public float GetIngredientFrequency()
    {
        return levelData.GetIngredientFrequencyAt(stageTime / levelData.levelDurationSec);
    }

    public void OnRecipeScored(SNS.Event.EventArgs evt)
    {
        RecipeSO recipe = evt.args[0] as RecipeSO;
        score += recipe.Points;
        ServiceLocator.GetEventSystem().TriggerEvent(EventNames.SCORE_CHANGED, new SNS.Event.EventArgs(score, recipe.Points));
    }

    public void OnGameStarted()
    {
        Debug.Log("STAGE_STARTED");
        gameRunning = true;
        score = 0;
        stageTime = 0.0f;
    }

    public int GetStarsCount()
    {
        if (score > 200)
        {
            return 3;
        }
        else if (score > 100)
        {
            return 2;
        }
        else if (score > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    #endregion

}
