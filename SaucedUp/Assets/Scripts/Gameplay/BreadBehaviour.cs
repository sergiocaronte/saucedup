﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadBehaviour : MonoBehaviour
{
    #region -- Members --
    private Transform tr;
    private Transform spawnPosition;
    [SerializeField] private int playerId = default;
    [SerializeField] private Transform foodHolderTr = default;
    [SerializeField] private float ballPushForce = default;
    [SerializeField] private SkinnedMeshRenderer renderer = default;

    public int[] ingredients { get; private set; } = default;

    private Char_Bread controller;

    private const float DEATH_DURATION = 2.75f;
    private bool isDead = false;
    private float lastDeathTime = 0;

    #endregion

    #region -- Unity Methods --
    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    void Awake()
    {
        controller = GetComponent<Char_Bread>();
        tr = transform;
        ingredients = new int[(int)IngredientType.Count];
        for (int i = 0; i < (int)IngredientType.Count; ++i)
        {
            ingredients[i] = 0;
        }
        controller.isPaused = true;
    }

    private void Start()
    {
        
    }


    void Update()
    {
        if (isDead)
        {           
            if (Time.time - lastDeathTime > DEATH_DURATION)
            {
                Debug.Log("Reset bready");
                ResetBread();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {    
        if (other.CompareTag("Ingredient"))
        {      
            var ingredient = other.GetComponent<IngredientBehaviour>();
            
            if (ingredient.IsSauce())
            {
                // replace the sauce
                ingredients[(int)IngredientType.Mayo] = 0;
                ingredients[(int)IngredientType.Jam] = 0;
                ingredients[(int)IngredientType.Mustard] = 0;
                ingredients[(int)IngredientType.PeanutButter] = 0;

                Debug.LogFormat("{0} added", ingredient.GetIngredientType().ToString());
                AddIngredient(ingredient.GetIngredientType(), ingredient.IsSauce());
               
            }
            else
            {
                // TODO Buzz sound
            }      
        }
        else if(other.CompareTag("Fire"))
        {
            Respawn(false);
        }
        else if(other.CompareTag("Player"))
        {

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody != null)
        {
            if (collision.rigidbody.CompareTag("Ball"))
            {
                var dir = collision.relativeVelocity;
                if (collision.rigidbody.velocity.magnitude > 2.0f)
                {                    
                    dir.y = 0.0f;
                    controller.Push(dir.normalized * ballPushForce);
                    // if carry any ingredient, drop it
                    if (HasIngredient())
                    {
                        DropTopIngredient();
                        // turn off the ball
                        collision.rigidbody.gameObject.SetActive(false);
                    }
                }
                else // if colliding to a stationary ball, kick it
                {
                    collision.rigidbody.AddForce(tr.forward * 10.0f, ForceMode.Impulse);
                }
            }
            else if (collision.rigidbody.CompareTag("Ingredient"))
            {
                var ingredient = collision.gameObject.GetComponent<IngredientBehaviour>();
                if (ingredient != null)
                {
                    AddIngredient(ingredient.GetIngredientType(), ingredient.IsSauce());
                    if (!ingredient.IsSauce())
                    {
                        ingredient.Destroy();
                    }
                }
                else
                {
                    Debug.LogError("Collided with an ingredient without IngredientBehaviour");
                }
            }
        }
    }

  #endregion

  #region -- Methods --
  public void Initialize(Transform spawnerPos, int myId)
  {
    spawnPosition = spawnerPos;
    playerId = myId;
    controller.playerId = playerId;
    controller.isPaused = false;
    Initialize();
  }

    public void Initialize()
    {
        tr.position = spawnPosition.position;
        tr.rotation = spawnPosition.rotation;
        Reset();
    }

    private void Reset()
    {
        for (int i = 0; i < (int)IngredientType.Count; ++i)
        {
            ingredients[i] = 0;
        }

        for (int i = foodHolderTr.childCount - 1; i >= 0; --i)
        {
            Destroy(foodHolderTr.GetChild(i).gameObject);
        }

        renderer.materials[0].SetColor("_Tintcolor", Color.black);
    }

    public void Respawn(bool isCrushed)
    {
        if (isCrushed)
        {
            controller.animator.SetBool("isSmushed", true);
        }
        else
        {
            controller.animator.Play("Death");
        }
        
        isDead = true;
        lastDeathTime = Time.time;
        controller.isPaused = true;
    }

    public void ResetBread()
    {
        Reset();
        controller.animator.SetBool("isSmushed", false);
        controller.animator.Play("Idle");
        controller.Respawn(spawnPosition.position);
        isDead = false;        
        controller.isPaused = false;
    }

    public bool IsSaucedUp()
    {
        foreach (IngredientType i in ingredients)
        {
            if ((int)i <= (int)IngredientType.Mustard)
            {
                return true;
            }
        }
        return false;
    }

    private void DropTopIngredient()
    {
        int ingredientCount = foodHolderTr.childCount;
        if (ingredientCount > 0)
        {
            var lastIng = foodHolderTr.GetChild(ingredientCount - 1);
            var ingredientBhv = lastIng.GetComponent<IngredientBehaviour>();
            ServiceLocator.GetEventSystem().TriggerEvent(EventNames.INGREDIENT_DROPPED, new SNS.Event.EventArgs(ingredientBhv, tr));
            lastIng.transform.parent = null;
            Destroy(lastIng.gameObject);

            int type = (int)ingredientBhv.GetIngredientType();
            ingredients[type] = Mathf.Clamp(ingredients[type] - 1, 0, ingredients[type]);

            //PrintIngredientList();
        }
    }

    private void AddIngredient(IngredientType type, bool isSauce)
    {
        ingredients[(int)type]++;
        if (!isSauce)
        {
            controller.SlowDown();

            int ingredientCount = foodHolderTr.childCount;
            GameObject ingredientBase = Resources.Load<GameObject>("Prefabs/IngredientsFace/" + RecipeSO.IngredientToString[(int)type]);
            GameObject ingredient = (GameObject)GameObject.Instantiate(ingredientBase, foodHolderTr);
            ingredient.name = RecipeSO.IngredientToString[(int)type];
            ingredient.transform.localPosition = new Vector3(0, 0, -0.016f * ingredientCount);
            ingredient.transform.localScale = Vector3.one;

            //PrintIngredientList();
        }
        else
        {
            switch(type)
            {
                case IngredientType.Jam:
                    {
                        renderer.materials[0].SetColor("_Tintcolor", new Color(0.5f, 0, 0.6f));
                        break;
                    }
                case IngredientType.Mustard:
                    {
                        renderer.materials[0].SetColor("_Tintcolor", new Color(1.0f, 0.6f, 0));
                        break;
                    }
                case IngredientType.Mayo:
                    {
                        renderer.materials[0].SetColor("_Tintcolor", new Color(0.7f, 0.7f, 0.6f));
                        break;
                    }
                case IngredientType.PeanutButter:
                    {
                        renderer.materials[0].SetColor("_Tintcolor", new Color(0.5f, 0.25f, 0.25f));
                        break;
                    }
            }
        }
    }

    private void PrintIngredientList()
    {
        string log = string.Format("{0} has:\n", name);
        for (int i = 0; i < (int)IngredientType.Count; ++i)
        {
            if (ingredients[i] > 0)
            {
                log += string.Format("{0}: {1}\n", RecipeSO.IngredientToString[i], ingredients[i]);
            }
        }
        Debug.Log(log);
    }

    private bool HasIngredient()
    {
        return foodHolderTr.childCount > 0;
    }

    private void OnEndGame()
    {
        controller.animator.Play("Death");
        controller.isPaused = true;
    }
    #endregion
}
