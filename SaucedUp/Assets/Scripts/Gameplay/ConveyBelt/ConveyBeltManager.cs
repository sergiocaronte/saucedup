﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyBeltManager : MonoBehaviour
{
     #region -- Members --
    [SerializeField] Transform spawnSpot = default;

    GameObjectPooler<IngredientBehaviour>[] ingredientsPool;

    Stack<IngredientType> ingredientStack;

    private float nextIngredientTime;
    private float ingredientSpawnFrequency;

    private bool isPaused = false;
    private bool gameEnded = false;
    #endregion

    #region -- Unity Methods --
    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.INGREDIENT_DROPPED, OnIngredientDropped);
        ServiceLocator.GetEventSystem().StartListening(EventNames.NEW_RECIPE_PICKED, OnNewRecipe);
        ServiceLocator.GetEventSystem().StartListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.INGREDIENT_DROPPED, OnIngredientDropped);
        ServiceLocator.GetEventSystem().StopListening(EventNames.NEW_RECIPE_PICKED, OnNewRecipe);
        ServiceLocator.GetEventSystem().StopListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    private void Awake()
    {
        ingredientStack = new Stack<IngredientType>();
        ingredientsPool = new GameObjectPooler<IngredientBehaviour>[(int)IngredientType.Count];
        // starting from cheese because we dont need pooling for sauces
        for (int i = (int)IngredientType.Cheese; i < (int)IngredientType.Count; ++i)
        {
            ingredientsPool[i] = new GameObjectPooler<IngredientBehaviour>();
            ingredientsPool[i].Init("Ingredients/" + RecipeSO.IngredientToString[i], Reset, transform);
        }
    }

    private void Start()
    {
        ingredientSpawnFrequency = 1.0f / LevelManager.Instance.GetIngredientFrequency();
        nextIngredientTime = ingredientSpawnFrequency;
    }

    void Update()
    {
        if (isPaused || gameEnded)
        {
            return;
        }

        nextIngredientTime -= Time.deltaTime;
        if (nextIngredientTime <= 0.0f)
        {
            SpawnIngredient();
            nextIngredientTime = 1.0f / LevelManager.Instance.GetIngredientFrequency();
        }
    }
  #endregion

    #region -- Methods --
    IngredientBehaviour SpawnIngredient()
    {
        bool recipeNull = RecipeManager.Instance.currentRecipe == null;
        bool recipeOnlySauce = !recipeNull && RecipeManager.Instance.currentRecipe.IsOnlySauce();
        bool randomChance = Random.value <= LevelManager.Instance.levelData.randomIngredientSpawningChance;

        // If only sauce or 50% chance of spawning randomly
        if ( recipeNull || recipeOnlySauce || randomChance)
        {
            return ingredientsPool[UnityEngine.Random.Range((int)IngredientType.Cheese, (int)IngredientType.Count)].GetNext();
        }
        else
        {
            // if recipe stack is empty, populate it again
            if (ingredientStack.Count == 0)
            {
                PopulateWithNewRecipe(RecipeManager.Instance.currentRecipe);
            }
            // get next ingredient from the stack
            return ingredientsPool[(int)ingredientStack.Pop()].GetNext();
        }
    }

    private void Reset(IngredientBehaviour ingredient)
    {
        ingredient.Spawn(spawnSpot.position);
    }

    private void OnIngredientDropped(SNS.Event.EventArgs evt)
    {
        IngredientBehaviour bhv = evt.args[0] as IngredientBehaviour;
        Transform spawnPosition = evt.args[1] as Transform;
        IngredientBehaviour newDrop = ingredientsPool[(int)bhv.GetIngredientType()].GetNext();
        if (newDrop != null)
        {
            newDrop.Pop(spawnPosition.position + new Vector3(0, 2, 0));
        }
    }

    private void OnNewRecipe(SNS.Event.EventArgs evt)
    {
        RecipeSO recipe = evt.args[0] as RecipeSO;
        PopulateWithNewRecipe(recipe);
    }

    private void PopulateWithNewRecipe(RecipeSO recipe)
    {
        if (recipe == null)
        {
            return;
        }

        ingredientStack.Clear();
        // skip sauces
        for (int i = (int)IngredientType.Cheese; i < (int)IngredientType.Count; ++i)
        {
            int amount = 0;
            while (amount < recipe.ingredients[i])
            {
                ingredientStack.Push((IngredientType)i);
                ++amount;
            }
        }

        // if stack it not empty, randomize it.
        if (ingredientStack.Count > 0)
        {
            // randomize stack
            for (int i = 0; i < ingredientStack.Count * 2; ++i)
            {
                if (Random.value < 0.5f)
                {
                    var popped = ingredientStack.Pop();
                    ingredientStack.Push(popped);
                }
            }
        }
    }

    private void OnEndGame()
    {
        gameEnded = true;
    }
  #endregion
}
