﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientBehaviour : MonoBehaviour
{
    #region -- Members
    Transform tr;
    Rigidbody rb;

      [SerializeField] IngredientType type = default;
    #endregion

    #region -- Unity Methods --
    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    private void Awake()
    {
        tr = transform;
        rb = GetComponent<Rigidbody>();
    }
    #endregion

    #region -- Methods --
    public void Spawn(Vector3 position)
    {
        transform.localPosition = position;
    }

    public bool IsSauce()
    {
        return ((int)type <= (int)IngredientType.Mustard);
    }

    public IngredientType GetIngredientType()
    {
        return type;
    }

    public void Destroy()
    {
        // TODO some cool fade animation
        gameObject.SetActive(false);
    }

    public void Pop(Vector3 pos)
    {
        tr.localPosition = pos;
        Vector3 dir = new Vector3(Random.value - 0.5f, 1.0f, Random.value - 0.5f);
        rb.AddForce(dir.normalized * 20.0f, ForceMode.Impulse);
    }

    private void OnEndGame()
    {
        if (rb != null)
        {
            rb.isKinematic = true;
        }
    }
    #endregion
}
