﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
  #region -- Members -- 
  private Transform tr;
  private Rigidbody rg;

  private float lifetime;
  [SerializeField] float maxLifetime = 50.0f;
  #endregion

  #region -- Unity Methods --
  // Start is called before the first frame update
  void Awake()
  {
    tr = transform;
    rg = GetComponent<Rigidbody>();
  }

  // Update is called once per frame
  void Update()
  {
    lifetime += Time.deltaTime;
    if (lifetime > maxLifetime)
    {
      gameObject.SetActive(false);
    }
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.CompareTag("Player"))
    {
      gameObject.SetActive(false);
    }
  }
  #endregion

  #region -- Methods --
  public void Launch(Vector3 origin, Vector3 direction, float speed)
  {
    lifetime = 0;
    tr.localPosition = origin;
    rg.velocity = direction * speed;
  }

  #endregion
}
