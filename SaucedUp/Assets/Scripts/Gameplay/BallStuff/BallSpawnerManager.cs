﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawnerManager : MonoBehaviour
{
    #region -- Members --
    [SerializeField] Transform[] spawnSpots = default;
    [SerializeField] Transform[] targets = default;

    GameObjectPooler<BallBehaviour> ballPool = default;

    private float nextBallTime;
    private float ballSpawnFrequency;

    private bool isPaused = false;
    private bool gameEnded = false;
    #endregion

    #region -- Unity Methods --
    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.STAGE_ENDED, OnEndGame);
    }

    private void Start()
    {
        ballSpawnFrequency = 1.0f / LevelManager.Instance.GetBallFrequency();
        nextBallTime = ballSpawnFrequency;

        ballPool = new GameObjectPooler<BallBehaviour>();
        ballPool.Init("Ball", Reset, transform);
        //ballPool.expandable = false;
    }

    void Update()
    {
        if (isPaused || gameEnded)
        {
            return;
        }

        nextBallTime -= Time.deltaTime;
        if (nextBallTime <= 0.0f)
        {
            SpawnBall();
            nextBallTime = 1.0f / LevelManager.Instance.GetBallFrequency();
        }
    }
    #endregion

    #region -- Methods --
    void SpawnBall()
    {
        ballPool.GetNext();
    }

    private void Reset(BallBehaviour ball)
    {
        Vector3 origin = spawnSpots[UnityEngine.Random.Range(0, spawnSpots.Length)].position;
        Vector3 target = targets[UnityEngine.Random.Range(0, targets.Length)].position;
        Vector3 randHeight = new Vector3(0, UnityEngine.Random.Range(1.0f, 1.25f), 0);
        Vector3 dir = (target + randHeight - origin).normalized;

        ball.Launch(origin, dir, UnityEngine.Random.Range(20.0f, 40.0f));
    }

    private void OnEndGame()
    {
        gameEnded = true;
    }
    #endregion
}
