﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IngredientType
{
  Mayo = 0,
  Jam = 1,
  PeanutButter = 2,
  Mustard = 3,
  Cheese = 4,
  Avocado = 5,
  Bacon = 6,
  Tomato = 7,
  Lettuce = 8,
  Tuna = 9,
  Ham = 10,
  Count = 11,
}

[CreateAssetMenu(fileName = "New Recipe", menuName = "SaucedUp/Recipe", order = 1)]
public class RecipeSO : ScriptableObject
{
    #region -- Members --
    public string RecipeName = "Recipe Name";
    public int Points = 0;

    [Header("Ingredients")]

    public int Mayo = 0;
    public int Jam = 0;
    public int PeanutButter = 0;
    public int Mustard = 0;
    public int Cheese = 0;
    public int Avocado = 0;
    public int Bacon = 0;
    public int Tomato = 0;
    public int Lettuce = 0;
    public int Tuna = 0;
    public int Ham = 0;

    public int[] ingredients { get; private set;  }

    public static string[] IngredientToString = {
    "Mayo",
    "Jam",
    "PeanutButter",
    "Mustard",
    "Cheese",
    "Avocado",
    "Bacon",
    "Tomato",
    "Lettuce",
    "Tuna",
    "Ham"
    };
    #endregion

    public RecipeSO()
    {
    }

    public void Initialize()
    {
        // Not scalable.. :(
        ingredients = new int[(int)IngredientType.Count];
        ingredients[(int)IngredientType.Mayo] = Mayo;
        ingredients[(int)IngredientType.Jam] = Jam;
        ingredients[(int)IngredientType.PeanutButter] = PeanutButter;
        ingredients[(int)IngredientType.Mustard] = Mustard;
        ingredients[(int)IngredientType.Cheese] = Cheese;
        ingredients[(int)IngredientType.Avocado] = Avocado;
        ingredients[(int)IngredientType.Bacon] = Bacon;
        ingredients[(int)IngredientType.Tomato] = Tomato;
        ingredients[(int)IngredientType.Lettuce] = Lettuce;
        ingredients[(int)IngredientType.Tuna] = Tuna;
        ingredients[(int)IngredientType.Ham] = Ham;

    }

    public bool IsOnlySauce()
    {
        // start from cheese because it is the first non sauce ingredient
        for (int i = (int)IngredientType.Cheese; i < (int)IngredientType.Count; ++i)
        {
            // if any ingredient present, recipe is not only sauce
            if (ingredients[i] > 0)
            {
                return false;
            }
        }
        return true;
    }


    public bool IsRecipeCorrect(BreadBehaviour p1, BreadBehaviour p2)
    {
        Debug.LogFormat("{0}, {1}, {2}", p1.ingredients.Length, p2.ingredients.Length, ingredients.Length);
        for (int i = 0; i < (int)IngredientType.Count; ++i)
        {
            Debug.LogFormat("{3}: {0} + {1} = {2}", p1.ingredients[i], p2.ingredients[i], ingredients[i], IngredientToString[i]);
            if (p1.ingredients[i] + p2.ingredients[i] != ingredients[i])
            {
                return false;
            }
        }

        return true;
    }
}
