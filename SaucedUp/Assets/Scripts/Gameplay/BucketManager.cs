﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketManager : MonoBehaviour
{
    private BucketBottomBehaviour bucketBottom;

    #region -- Members --


    #endregion

    #region -- Unity Methods --
    // Start is called before the first frame update
    void Start()
    {
        bucketBottom = GameObject.FindGameObjectWithTag("BucketFloor").GetComponent<BucketBottomBehaviour>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag.Contains("Player") && !bucketBottom.isOpen)
        {
            if (other.GetComponent<Char_Bread>().isHeadButting)
            {
                // Purge the bucket muhahahahahahahahaha
                bucketBottom.Open();

                Debug.Log("Bucket is open");
            }            
        }
    }

    #endregion

    #region -- Methods --

    #endregion
}
