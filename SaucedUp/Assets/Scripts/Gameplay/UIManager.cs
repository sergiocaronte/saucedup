﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    #region -- Members --

    [SerializeField] GameObject victoryScreen;
    [SerializeField] GameObject recipeWidget;
    [SerializeField] NoBuenoVisibility noBueno;
    [SerializeField] PointsVisibility pointsVisibility;
    [SerializeField] WonderfulVisibility wonderfulVisibility;

    // Start is called before the first frame update

    #endregion

    #region -- Unity Methods --
    void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.STAGE_ENDED, OnEndGame);
        ServiceLocator.GetEventSystem().StartListening(EventNames.RECIPE_SCORED, RecipeScored);
        ServiceLocator.GetEventSystem().StartListening(EventNames.RECIPE_FAILED, RecipeFailed);
        ServiceLocator.GetEventSystem().StartListening(EventNames.RECIPE_FAILED, RecipeFinished);
        ServiceLocator.GetEventSystem().StartListening(EventNames.RECIPE_SCORED, WRecipeFinished);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.STAGE_ENDED, OnEndGame);
        ServiceLocator.GetEventSystem().StopListening(EventNames.RECIPE_SCORED, RecipeScored);
        ServiceLocator.GetEventSystem().StopListening(EventNames.RECIPE_FAILED, RecipeFailed);
        ServiceLocator.GetEventSystem().StopListening(EventNames.RECIPE_FAILED, RecipeFinished);
        ServiceLocator.GetEventSystem().StopListening(EventNames.RECIPE_SCORED, WRecipeFinished);
    }
    #endregion

    #region -- Methods --
    private void OnEndGame()
    {
        StartCoroutine(ShowScoreSequence());
    }

    private IEnumerator ShowScoreSequence()
    {
        yield return new WaitForSeconds(4);
        victoryScreen.SetActive(true);
        ServiceLocator.GetEventSystem().TriggerEvent(EventNames.SHOW_SCORE);
        recipeWidget.SetActive(false);
    }

    public void RecipeFailed(SNS.Event.EventArgs evt)
    {
        pointsVisibility.RecipeFailed(evt);
    }

    public void RecipeScored(SNS.Event.EventArgs evt)
    {
        pointsVisibility.RecipeScored(evt);
    }

    public void RecipeFinished(SNS.Event.EventArgs evt)
    {
        noBueno.RecipeFinished(evt);
    }


    public void WRecipeFinished(SNS.Event.EventArgs evt)
    {
        wonderfulVisibility.RecipeFinished(evt);
    }
    #endregion
}
