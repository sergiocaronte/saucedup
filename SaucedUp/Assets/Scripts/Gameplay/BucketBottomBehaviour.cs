﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketBottomBehaviour : MonoBehaviour
{
    public bool isOpen { get; private set; }

    private float lastOpenTime = 0;
    private const float BUCKET_OPEN_TIME = 3.0f;
    private const float OPEN_BUCKET_OFFSET = 20.0f;

    private Vector3 startPos;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            if (Time.time - lastOpenTime > BUCKET_OPEN_TIME)
            {
                isOpen = false;
                transform.position = startPos;
                Debug.Log("Bucket is closed");
            }
        }
    }

    public void Open()
    {
        if (!isOpen) isOpen = true;
        lastOpenTime = Time.time;

        transform.position = new Vector3(
                    transform.position.x + OPEN_BUCKET_OFFSET,
                    transform.position.y,
                    transform.position.z);
    }
}
