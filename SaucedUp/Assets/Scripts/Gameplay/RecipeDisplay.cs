﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecipeDisplay : MonoBehaviour
{
    #region -- Members
    public Image[] recipeImages = default;

    #endregion

    #region -- Unity Members
    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.NEW_RECIPE_PICKED, OnNewRecipe);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.NEW_RECIPE_PICKED, OnNewRecipe);
    }

    void Start()
    {
        recipeImages = GetComponentsInChildren<Image>(true);
    }
    #endregion

    #region -- Members
    public void OnNewRecipe(SNS.Event.EventArgs evt)
    {   
        RecipeSO recipe = evt.args[0] as RecipeSO;
        int index = (int)evt.args[1];

        for (int i = 0; i < recipeImages.Length; i++)
        {
            if (i == index)
            {
                recipeImages[i].enabled = true;
                Debug.Log("Enabling image " + recipeImages[i].name);
                recipeImages[i].gameObject.SetActive(true);
            }
            else
            {
                recipeImages[i].enabled = false;
                recipeImages[i].gameObject.SetActive(false);

            }
        }
    }

    #endregion
}
