﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TuningData", menuName = "SaucedUp/TuningData", order = 1)]
public class LevelTuningData : ScriptableObject
{
    public float levelDurationSec = 120.0f;
    public float randomIngredientSpawningChance = 0.5f;
    public AnimationCurve ballFrequencyCurve;
    public AnimationCurve ingredientFrequencyCurve;

    public float GetBallFrequencyAt(float time)
    {
        return ballFrequencyCurve.Evaluate(time);
    }

    public float GetIngredientFrequencyAt(float time)
    {
        return ingredientFrequencyCurve.Evaluate(time);
    }
}
