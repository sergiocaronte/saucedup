﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crusher : MonoBehaviour
{
    #region -- Members --
    [SerializeField] private bool isPlayer1Below = false;
    [SerializeField] private bool isPlayer2Below = false;

    [SerializeField] private BreadBehaviour p1;
    [SerializeField] private BreadBehaviour p2;
    


    private Transform tr;
    #endregion

    #region -- Unity Methods --

    // Start is called before the first frame update
    void Start()
    {
        tr = transform;
        p1 = GameObject.Find("Player1").GetComponent<BreadBehaviour>();
        p2 = GameObject.Find("Player2").GetComponent<BreadBehaviour>();

        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Equals("Player1"))
        {
            isPlayer1Below = true;
        }
        else if (other.name.Equals("Player2"))
        {
            isPlayer2Below = true;
        }

        if (isPlayer1Below && isPlayer2Below)
        {
            Squash();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name.Equals("Player1"))
        {
            isPlayer1Below = false;
        }
        else if (other.name.Equals("Player2"))
        {
            isPlayer2Below = false;
        }
    }
    #endregion

    #region -- Methods
    private void  Squash()
    {
        StartCoroutine(Crush());
        isPlayer1Below = isPlayer2Below = false;
        RecipeManager.Instance.CheckRecipe(p1, p2);
    }

    IEnumerator Crush()
    {
        float offset = 0.0f;
        while (offset < 2.0f)
        {
            float inc = Time.deltaTime * 10.0f;
            offset += inc;
            tr.Translate(0, -inc, 0);
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(Reset());
        Object poof = Resources.Load("CFX_MagicPoof");

        GameObject p1Instance = (GameObject)Instantiate(poof);
        GameObject p2Instance = (GameObject)Instantiate(poof);

        p1Instance.transform.position = p1.transform.position + (Vector3.up / 2);
        p2Instance.transform.position = p2.transform.position + (Vector3.up / 2);

        p1Instance.GetComponent<ParticleSystem>().Play();
        p2Instance.GetComponent<ParticleSystem>().Play();
    }

    IEnumerator Reset()
    {
        float offset = 0.0f;
        while (offset < 2.0f)
        {
            float inc = Time.deltaTime * 0.5f;
            offset += inc;
            tr.Translate(0, inc, 0);
            yield return new WaitForEndOfFrame();
        }
    }
    #endregion
}
