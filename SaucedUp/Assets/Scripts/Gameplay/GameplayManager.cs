﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : Singleton<GameplayManager>
{
    #region -- Members --
    [SerializeField] List<BreadBehaviour> players = default;
    [SerializeField] Transform[] spawnSpots = default;


    public RecipeManager recipeMgr { get; private set; }
    #endregion

    #region -- Unity Methods --
    override protected void Init()
    {
        recipeMgr = GetComponent<RecipeManager>();
    }

    private void OnEnable()
    {
        ServiceLocator.GetEventSystem().StartListening(EventNames.STARTING_COUNTDOWN_FINISHED, StartGame);
    }

    private void OnDisable()
    {
        ServiceLocator.GetEventSystem().StopListening(EventNames.STARTING_COUNTDOWN_FINISHED, StartGame);
    }

    private void Start()
    {
        ServiceLocator.GetEventSystem().TriggerEvent(EventNames.NEW_ROUND_STARTED);
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Debug.Log("Pressed ESC");
            ServiceLocator.GetEventSystem().TriggerEvent(EventNames.PAUSE_PRESSED, new SNS.Event.EventArgs());
        }
    }
    #endregion

    #region -- Methods --
    public void StartGame()
     {
        Debug.Log("STARTING_COUNTDOWN_FINISHED");
        int index = 0;
        foreach(var player in players)
        {
          player.Initialize(spawnSpots[index], index++);
        }

        recipeMgr.Initialize();
        ServiceLocator.GetEventSystem().TriggerEvent(EventNames.STAGE_STARTED);

    }
     #endregion
}
