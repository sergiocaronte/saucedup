﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTrigger : MonoBehaviour
{
    /// <summary>
    /// The event name that will be fired when TriggerEvent is called
    /// </summary>
    public EventNames _eventName;

    /// <summary>
    /// Fire the event currently set in _eventName
    /// </summary>
    public void TriggerEvent()
    {
        TriggerEvent(_eventName);
    }

    /// <summary>
    /// Fire an event using the EventSystem singleton
    /// </summary>
    /// <param name="eventName"></param>
    public void TriggerEvent(EventNames eventName)
    {
        ServiceLocator.GetEventSystem().TriggerEvent(eventName);
    }
}
