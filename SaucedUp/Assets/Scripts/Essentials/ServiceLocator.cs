﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

using SNS;

public class ServiceLocator : Singleton<ServiceLocator>
{
  static AudioManager sAudioManager;
  //static DataManager sDataManager;
  static EventSystem sEventSystem;

  protected override void Init()
  {
    print("Service Locator Init");
    DontDestroyOnLoad(this);

    sAudioManager = new AudioManager();
    sAudioManager.Init();

    //sDataManager = new DataManager();
    //sDataManager.Init();

    sEventSystem = new EventSystem();
  }

  /*public static AudioManager GetAudioManager()
  {
      Assert.IsNotNull(sAudioManager, "sAudioManager is null.");
      return sAudioManager;
  }*/

  /*public static DataManager GetDataManager()
  {
    Assert.IsNotNull(sDataManager , "sDataManager is null.");
    return sDataManager;
  }*/

  public static EventSystem GetEventSystem()
  {
    Assert.IsNotNull(sEventSystem, "sEventSystem is null.");
    return sEventSystem;
  }
}
