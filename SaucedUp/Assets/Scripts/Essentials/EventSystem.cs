﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using System.Runtime.CompilerServices;
[assembly: InternalsVisibleToAttribute("ServiceLocator")]

namespace SNS
{
  public class EventSystem
  {
    private Dictionary<EventNames, Event> events;

    internal EventSystem()
    {
      if (events == null)
        events = new Dictionary<EventNames, Event>();
    }

    // Caster known events
    public void StartListening(EventNames eventName, Action<GameObject, Event.EventArgs> action)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
      {
        uevent.AddObserver(action);
      }
      else
      {
        uevent = new Event();
        uevent.AddObserver(action);
        events.Add(eventName, uevent);
      }
    }

    public void StopListening(EventNames eventName, Action<GameObject, Event.EventArgs> action)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
        uevent.RemoveObserver(action);
    }

    // Anonymous events
    public void StartListening(EventNames eventName, Action<Event.EventArgs> action)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
      {
        uevent.AddObserver(action);
      }
      else
      {
        uevent = new Event();
        uevent.AddObserver(action);
        events.Add(eventName, uevent);
      }
    }
       
    public void StopListening(EventNames eventName, Action<Event.EventArgs> action)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
        uevent.RemoveObserver(action);
    }

    // Trigger events
    public void StartListening(EventNames eventName, Action action)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
      {
        uevent.AddObserver(action);
      }
      else
      {
        uevent = new Event();
        uevent.AddObserver(action);
        events.Add(eventName, uevent);
      }
    }

    public void StopListening(EventNames eventName, Action action)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
        uevent.RemoveObserver(action);
    }

    public void TriggerEvent(EventNames eventName, GameObject obj, Event.EventArgs args)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
        uevent.Invoke(obj, args);
    }

    public void TriggerEvent(EventNames eventName, Event.EventArgs args)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
        uevent.Invoke(null, args);
    }

    public void TriggerEvent(EventNames eventName)
    {
      Event uevent = null;
      if (events.TryGetValue(eventName, out uevent))
        uevent.Invoke(null, Event.EventArgs0);
    }
  }
}
