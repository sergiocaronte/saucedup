﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EventNames : int
{
    STAGE_STARTED, // Players are spawned, recepie is selected, gameplay timer starts
    STAGE_ENDED,
    SHOW_SCORE,
    RECIPE_SCORED,
    RECIPE_FAILED,
    PLAYER_RESPAWNED,
    INGREDIENT_TAKEN,
    SCORE_CHANGED,
    NEW_RECIPE_PICKED,
    INGREDIENT_DROPPED,
    STARTING_COUNTDOWN_FINISHED, // Triggered when countdown finishes, next is STAGE_STARTED
    NEW_ROUND_STARTED, // Triggered every time a new round is started, next is the countdown and STARTING_COUNTDOWN_FINISHED
    PAUSE_PRESSED
}
