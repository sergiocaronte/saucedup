﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectPooler<T> where T : MonoBehaviour
{
  public GameObject pooledObject;
  public int poolAmount = 50;
  public bool expandable = true;

  List<T> pool;
  public delegate void ResetAction(T go);
  ResetAction reset;

  public GameObjectPooler()
  { }

  public void Init(string resourceName, ResetAction r, Transform parent)
  {
    reset = r;
    pool = new List<T>();
    pooledObject = Resources.Load<GameObject>("Prefabs/" + resourceName);

    for (int i = 0; i < poolAmount; i++)
    {
      GameObject obj = (GameObject)GameObject.Instantiate(pooledObject, parent);
      obj.name = "(Pool)" + resourceName + "_" + i.ToString();
      T component = obj.GetComponent<T>();
      reset(component);
      obj.SetActive(false);
      pool.Add(component);
    }
	}

  public T GetNext()
  {
    if (pool == null)
    {
      return null;
    }

    for (int i = 0; i < pool.Count; i++)
    {
      if (!pool[i].gameObject.activeInHierarchy)
      {
        reset(pool[i]);
        pool[i].gameObject.SetActive(true);
        return pool[i];
      }
    }

    if (expandable)
    {
      GameObject obj = (GameObject)GameObject.Instantiate(pooledObject, pool[0].transform.parent);
      obj.name = "(Pool Exp)" + pooledObject.name + "_" + pool.Count.ToString();
      T component = obj.GetComponent<T>();
      reset(component);
      pool.Add(component);
      return component;
    }

    return null;
  }
}
