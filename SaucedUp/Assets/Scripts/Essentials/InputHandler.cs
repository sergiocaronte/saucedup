﻿using UnityEngine;
using System.Collections;

public enum CommandCode : uint
{
    NONE =            0,
    UP =              1,
    DOWN =            1 << 1,   // 2
    LEFT =            1 << 2,   // 4
    RIGHT =           1 << 3,   // 8
    CONFIRM =         1 << 4,   // 16   Cross
    CANCEL =          1 << 5,   // 32   Circle
    OPTION =          1 << 6,   // 64   Triangle 
    EXTRA =           1 << 7,   // 128  Square
    MOUSEMOVE =       1 << 8,   // 256
    MOUSELEFTPRESS =  1 << 9,   // 512
    MOUSERIGHTPRESS = 1 << 10,  // 1024
    START =           1 << 11,  // 2048  PS' Options
    SHARE =           1 << 12,  // 4096  PS' Share
    HOME  =           1 << 13,  // 8192  PS button
    L1 =              1 << 14,  // 16384
    R1 =              1 << 15,  // 32768
}

public class InputHandler : Singleton<InputHandler>
{
  Vector2 mousePos;
  uint frameCmd;

  bool upReleased = true;
  bool downReleased = true;
  bool leftReleased = true;
  bool rightReleased = true;

  public void Start()
  {
    mousePos = Input.mousePosition;
    frameCmd = (uint)CommandCode.NONE;
  }

  /// <summary>
  /// Check if the button was pressed.
  /// </summary>
  /// <param name="cc">Button's CommandCode</param>
  public bool IsPressed(CommandCode cc)
  {
    bool pressed = ((frameCmd & (uint)cc) > 0);
    // if key pressed, we clean its buffer
    if (pressed)
    {
      frameCmd &= (uint.MaxValue - (uint)cc);
    }
    return pressed;
  }

  /// <summary>
  /// Virtually press a button
  /// </summary>
  /// <param name="cc">Button's CommandCode to be considered pressed</param>
  public void Press(CommandCode cc)
  {
    frameCmd |= (uint)cc;
  }
    
  public uint Evaluate()
  {
    frameCmd = (uint)CommandCode.NONE;
    if (Input.GetAxis("DPADY") == 1.0f || Input.GetKeyDown(KeyCode.W) || Input.GetAxis("XDPADY") == 1.0f)
    {
      if (upReleased)
      {
        upReleased = false;
        frameCmd |= (uint)CommandCode.UP;
      }
    }
    else
    {
      upReleased = true;
    }


    if (Input.GetAxis("DPADY") == -1.0f || Input.GetKeyDown(KeyCode.S) || Input.GetAxis("XDPADY") == -1.0f)
    {
      if (downReleased)
      {
        downReleased = false;
        frameCmd |= (uint)CommandCode.DOWN;
      }
    }
    else
    {
      downReleased = true;
    }

    if (Input.GetAxis("DPADX") == -1.0f || Input.GetKeyDown(KeyCode.A) || Input.GetAxis("XDPADX") == -1.0f)
    {
      if (leftReleased)
      {
        leftReleased = false;
        frameCmd |= (uint)CommandCode.LEFT;
      }
    }
    else
    {
      leftReleased = true;
    }

    if (Input.GetAxis("DPADX") > 0.1f || Input.GetKeyDown(KeyCode.D) || Input.GetAxis("XDPADX") == 1.0f)
    {
      if (rightReleased)
      {
        rightReleased = false;
        frameCmd |= (uint)CommandCode.RIGHT;
      }
    }
    else
    {
      rightReleased = true;
    }

    if (Input.GetButtonDown("CROSS") || Input.GetKeyDown(KeyCode.Return))
    {
      frameCmd |= (uint)CommandCode.CONFIRM;
    }

    if (Input.GetButtonDown("CIRCLE") || Input.GetKeyDown(KeyCode.Space))
    {
      frameCmd |= (uint)CommandCode.CANCEL;
    }

    if (Input.GetButtonDown("TRIANGLE"))
    {
      frameCmd |= (uint)CommandCode.OPTION;
    }

    if (Input.GetButtonDown("SQUARE"))
    {
      frameCmd |= (uint)CommandCode.EXTRA;
    }

    if (GetMouseDelta().magnitude > 0)
    {
      frameCmd |= (uint)CommandCode.MOUSEMOVE;
    }

    if (Input.GetMouseButtonDown(0))
    {
      frameCmd |= (uint)CommandCode.MOUSELEFTPRESS;
    }

    if (Input.GetMouseButtonDown(1))
    {
      frameCmd |= (uint)CommandCode.MOUSERIGHTPRESS;
    }

    if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
    {
      frameCmd |= (uint)CommandCode.EXTRA;
    }

    if (Input.GetButtonDown("START") || Input.GetKeyDown(KeyCode.Escape))
    {
      frameCmd |= (uint)CommandCode.START;
    }

    if (Input.GetButtonDown("SHARE"))
    {
      frameCmd |= (uint)CommandCode.SHARE;
    }

    if (Input.GetButtonDown("HOME"))
    {
      frameCmd |= (uint)CommandCode.HOME;
    }

    if (Input.GetButtonDown("LSHOULDER"))
    {
      frameCmd |= (uint)CommandCode.L1;
    }

    if (Input.GetButtonDown("RSHOULDER"))
    {
      frameCmd |= (uint)CommandCode.R1;
    }

    return frameCmd;
  }

  public Vector2 GetMouseDelta()
  {
    Vector2 currMousePos = Input.mousePosition;
    Vector2 delta = new Vector2(currMousePos.x - mousePos.x, currMousePos.y - mousePos.y);
    mousePos = currMousePos;
    return delta;
  }
}
