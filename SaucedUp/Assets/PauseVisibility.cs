﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseVisibility : MonoBehaviour
{
    private bool isPaused = false;

    public void TogglePause()
    {
        SetPauseVisibility(!isPaused);
    }

    public void SetPauseVisibility(bool newPaused)
    {
        isPaused = newPaused;
        gameObject.SetActive(isPaused);
        if (isPaused)
        {
            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }
}
